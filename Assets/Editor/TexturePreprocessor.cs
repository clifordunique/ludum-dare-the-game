﻿using UnityEditor;

/// <summary>
/// Applies default texture import settings.
/// </summary>
public sealed class TexturePreprocessor : AssetPostprocessor
{
    void OnPreprocessTexture() {
        TextureImporter importer = assetImporter as TextureImporter;
        if (importer.textureType == TextureImporterType.Sprite && importer.spritePackingTag == "Default") {
            return;
        }

        importer.textureType = TextureImporterType.Sprite;
        importer.mipmapEnabled = false;
        importer.spritePixelsPerUnit = 1;
        importer.filterMode = UnityEngine.FilterMode.Point;
        importer.textureCompression = TextureImporterCompression.Uncompressed;
        importer.spritePackingTag = "Default";
    }
}
