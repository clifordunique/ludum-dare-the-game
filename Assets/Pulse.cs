﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pulse : MonoBehaviour
{
    Image _image;

    void Awake() {
        _image = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update() {
        float alpha = Mathf.Cos(Time.unscaledTime * 10) / 2 + 0.5f;
        _image.color = new Color(1.0f, 1.0f, 0.0f, alpha);
    }
}
