﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TitleEffect : MonoBehaviour
{

    // Use this for initialization
    void Start() {
		
    }
	
    // Update is called once per frame
    void Update() {
        float hue = Time.unscaledTime / 2 % 1.0f;
        Color color = Color.HSVToRGB(hue, 1.0f, 1.0f);
        color = new Color(color.r, color.g, color.b, 0.5f);
        GetComponent<Outline>().effectColor = color;
    }
}
