﻿using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameStatsText : MonoBehaviour
{
    [SerializeField]
    Transform _itemParent;

    List<ResultItem> _items = new List<ResultItem>();

    void Awake() {
        for (int i = 0; i < _itemParent.childCount; ++i) {
            _items.Add(_itemParent.GetChild(i).GetComponent<ResultItem>());
        }
    }

    void Update() {
        Stats stats = GameController.Instance.Stats;
        _items[0].Set(string.Format("Programming: {0}", stats.GameProgramming), "icon_programming");
        _items[1].Set(string.Format("Art: {0}", stats.GameArt), "icon_art");
        _items[2].Set(string.Format("Audio: {0}", stats.GameAudio), "icon_audio");
        _items[3].Set(string.Format("Design: {0}", stats.GameDesign), "icon_design");
        _items[4].Set(string.Format("Community: {0}", stats.GameCommunity), "icon_community");
        _items[5].Set(string.Format("Bugs: {0}", stats.GameBugs), "icon_bugs");
    }
}
