﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using HarmonicUnity;

public class DesignButton : MyButton, IPointerClickHandler
{
    public void OnPointerClick(PointerEventData eventData) {
        StartCoroutine(DoOnPointerClick());
    }

    IEnumerator DoOnPointerClick() {
        yield return DefaultCanvas.GetDialogLock();
        GameController.Pause();

        int result = -1;
        yield return StartCoroutine(ChoiceBox.Show("Should I work on some design ideas?", val => {
            result = val;
        }));

        if (result == 0) {
            GameController.Unpause();
            DefaultCanvas.ReleaseDialogLock();
            yield break;
        }

        int stop = -1;
        yield return StartCoroutine(ChoiceBox.AskStopAction(val => {
            stop = val;
        }));

        if (stop == 1) {
            GameController.ChangeAction(GameController.GameAction.Design);
        }

        GameController.Unpause();
        DefaultCanvas.ReleaseDialogLock();

        yield break;
    }
}
