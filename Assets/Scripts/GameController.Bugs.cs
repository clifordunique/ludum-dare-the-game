﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HarmonicUnity;

public partial class GameController : MonoBehaviour
{
    IEnumerator GameActionBugs() {
        AudioSource audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.clip = Resources.Load<AudioClip>("typing_normal");
        audioSource.loop = true;
        audioSource.Play();
        CharacterSprites.Switch("computer");

        Cleanup = () => {
            audioSource.Stop();
            Destroy(audioSource);
        };
        ActionName = "debugging";

        float rand = UnityEngine.Random.value;

        float progress = 0.0f;
        while (true) {
            yield return null;
            progress += Time.deltaTime;

            if (rand <= 1.0f) {
                // Normal feature.
                if (progress >= 5.0f) {
                    yield return DefaultCanvas.GetDialogLock();
                    GameController.Pause();

                    GameController.StopAction(false);

                    int bugs = Mathf.RoundToInt(3 * Stats.CharacterProductivity());
                    bugs = Mathf.Min(bugs, Stats.GameBugs);
                    Stats.GameBugs -= bugs;
                    yield return ResultBox.Show(
                        "Success!  I fixed some bugs in my game!",
                        new[] { string.Format("-{0} Bugs", bugs) },
                        new[] { "icon_bugs" }
                    );

                    DefaultCanvas.ReleaseDialogLock();
                    GameController.Unpause();
                    yield break;
                }
            }
        }
    }
}
