﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HarmonicUnity;

public class ResultBox : GenericBox
{
    public static ResultBox Instance;

    [SerializeField]
    Transform _itemParent;

    [SerializeField]
    AudioClip _bonusSound;

    List<ResultItem> _items = new List<ResultItem>();

    public static IEnumerator Show(string text, string[] bonuses, string[] icons) {
        foreach (ResultItem item in Instance._items) {
            item.gameObject.SetActive(false);
        }

        Instance.gameObject.SetActive(true);

        yield return Instance.StartCoroutine(Instance.ShowText(text));

        for (int i = 0; i < bonuses.Length; ++i) {
            Instance._items[i].Set(bonuses[i], icons[i]);
            Instance._items[i].gameObject.SetActive(true);
            AudioManager.PlaySound(Instance._bonusSound);

            yield return new WaitForSecondsRealtime(0.75f);
        }


        while (!Input.GetMouseButtonDown(0)) {
            yield return null;
        }
        Instance.gameObject.SetActive(false);
        yield break;
    }

    protected override void Awake() {
        base.Awake();
        Instance = this;
        for (int i = 0; i < _itemParent.childCount; ++i) {
            _items.Add(_itemParent.GetChild(i).GetComponent<ResultItem>());
        }
    }
}
