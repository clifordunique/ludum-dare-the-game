﻿using UnityEngine;
using System;
using System.Collections.Generic;
using HarmonicUnity;

public class Genre
{
    public class Requirement
    {
        public Func<bool> Callback;

        public string Text;

        public Requirement(string text, Func<bool> callback) {
            Callback = callback;
            Text = text;
        }

        public string FormattedText() {
            string color = "#ff0000";
            if (Callback()) {
                color = "#00ff00";
            }
            return string.Format("<color={0}>{1}</color>", color, Text);
        }
    }

    public string Description;

    public string Name;

    public string TextName;

    public static Genre[] Genres = null;

    public int Index;

    public List<Requirement> Requirements = new List<Requirement>();

    public int Score() {
        Stats stats = GameController.Instance.Stats;

        List<int> nums = new List<int>();
        nums.Add(stats.GameArt);
        nums.Add(stats.GameAudio);
        nums.Add(stats.GameCommunity);
        nums.Add(stats.GameDesign);
        nums.Add(stats.GameProgramming);

        // Lowest category gets added more times to pull score down.
        int lowest = 10000000;
        lowest = Mathf.Min(stats.GameArt, lowest);
        lowest = Mathf.Min(stats.GameAudio, lowest);
        lowest = Mathf.Min(stats.GameCommunity, lowest);
        lowest = Mathf.Min(stats.GameDesign, lowest);
        lowest = Mathf.Min(stats.GameProgramming, lowest);

        nums.Add(lowest);
        nums.Add(lowest);

        switch (Index) {
        case 0:
            break;
        case 1:
            break;
        case 2:
            // Audio game
            nums.Add(stats.GameAudio);
            nums.Add(stats.GameAudio);
            nums.Add(stats.GameAudio);
            nums.Add(stats.GameAudio);
            break;
        case 3:
            // Community
            break;
        default:
            Utils.Assert(false);
            break;
        }

        int sum = 0;
        foreach (int num in nums) {
            sum += num;
        }
        float average = ((float)sum) / nums.Count;


        float result = average;
        // Subtract for bugs.
        if (Index == 1) {
            result -= stats.GameBugs * 10;
        } else {
            result -= stats.GameBugs * 5;
        }

        // Divide by 2 if unfinished.
        if (!stats.Genre.MeetsRequirements()) {
            result /= 2;
        }

        result = Mathf.Clamp(result, 1, 100);
        return Mathf.RoundToInt(result);
    }

    public bool MeetsRequirements() {
        foreach (Requirement requirement in Requirements) {
            if (!requirement.Callback()) {
                return false;
            }
        }
        return true;
    }

    public static Genre Get(int index) {
        if (Genres == null) {
            Genres = new Genre[8];
            for (int i = 0; i < Genres.Length; ++i) {
                Genres[i] = new Genre();
                Genres[i].Index = i;
            }

            Genres[0].Name = "Platformer";
            Genres[0].TextName = "Platformer";
            Genres[0].Description = "The most straightforward game type.  Great for beginners.\n"
            + "Requirements: 50 Programming, 50 Art\n"
            + "- Simple but boring";
            Genres[0].Requirements.Add(new Requirement("Programming - 50", () => {
                return GameController.Instance.Stats.GameProgramming >= 50;
            }));
            Genres[0].Requirements.Add(new Requirement("Art - 50", () => {
                return GameController.Instance.Stats.GameArt >= 50;
            }));

            Genres[1].Name = "Arcade";
            Genres[1].TextName = "Arcade Game";
            Genres[1].Description = "Relies on very tight gameplay.\n"
            + "Requirements: 75 Programming, 75 Design\n"
            + "- Needs to be bug-free to score highly";
            Genres[1].Requirements.Add(new Requirement("Programming - 75", () => {
                return GameController.Instance.Stats.GameProgramming >= 75;
            }));
            Genres[1].Requirements.Add(new Requirement("Design - 75", () => {
                return GameController.Instance.Stats.GameDesign >= 75;
            }));

            Genres[2].Name = "Rhythm";
            Genres[2].TextName = "Rhythm Game";
            Genres[2].Description = "Heavily dependent on audio scores.\n"
            + "Requirements: 50 Programming, 150 Audio\n"
            + "- Audio affects your scoring more highly";
            Genres[2].Requirements.Add(new Requirement("Programming - 50", () => {
                return GameController.Instance.Stats.GameProgramming >= 50;
            }));
            Genres[2].Requirements.Add(new Requirement("Audio - 150", () => {
                return GameController.Instance.Stats.GameAudio >= 150;
            }));

            Genres[3].Name = "MMORTSRPG";
            Genres[3].TextName = "Massively Multiplayer Online Real Time Strategy Role Playing Game";
            Genres[3].Description = "...are you crazy?\n"
            + "Requirements: 200 Programming, 50 Community\n"
            + "- You're probably not going to sleep at all";
            Genres[3].Requirements.Add(new Requirement("Programming - 200", () => {
                return GameController.Instance.Stats.GameProgramming >= 200;
            }));
            Genres[3].Requirements.Add(new Requirement("Community - 50", () => {
                return GameController.Instance.Stats.GameCommunity >= 50;
            }));
        }

        return Genres[index];
    }

    public static string RandomName(int index) {

        string[] prefixes = null;
        string[] suffixes = null;

        switch (index) {
        case 0:
            prefixes = new[] {
                "Super Maria",
                "Monic the Mole",
                "Stone Man",
                "Ninja Garden",
                "Retroidvania",
                "Bomberguy"
            };
            break;
        case 1:
            prefixes = new[] { "Pong", "Rubble Wobble", "Generic Beat-em-up", "Bullet Hell", "Pavement Fighter" };
            break;
        case 2:
            prefixes = new[] { "Bongo", "Tabla", "Dubstep", "Chiptune", "Bagpipe" };
            break;
        case 3:
            prefixes = new[] { "World of ", "The Legend of ", "Forgotten ", "Eternal ", "Epic " };
            break;
        }

        switch (index) {
        case 0:
            suffixes = new[] { " Maker", " Saga", " 3000", " Extreme", " Galaxy", " in Space", " 5", " HD Remix" };
            break;
        case 1:
            suffixes = new[] {
                ": Championship Edition",
                " Unlimited",
                ": Fight for the Future",
                ": Online Edition",
                " MAX",
                " Turbo"
            };
            break;
        case 2:
            suffixes = new[] { " Hero", " Band", " Karaoke", " Idol", " Raver" };
            break;
        case 3:
            suffixes = new[] { "Trains", "Skyscraper Tycoons", "Ninja Pirates", "Giant Robots", "Black Holes" };
            break;
        }

        int p = UnityEngine.Random.Range(0, prefixes.Length);
        int s = UnityEngine.Random.Range(0, suffixes.Length);

        return prefixes[p] + suffixes[s];
    }
}
