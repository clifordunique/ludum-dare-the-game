﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HarmonicUnity;

public partial class GameController : MonoBehaviour
{
    IEnumerator GameActionCook() {
        AudioSource audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.clip = Resources.Load<AudioClip>("cooking");
        audioSource.loop = true;
        audioSource.Play();
        CharacterSprites.Switch("cook");

        Cleanup = () => {
            audioSource.Stop();
            Destroy(audioSource);
        };
        ActionName = "cooking";

        float rand = UnityEngine.Random.value;

        float progress = 0.0f;
        while (true) {
            yield return null;
            progress += Time.deltaTime;

            if (rand <= 1.0f) {
                // Normal feature.
                if (progress >= 6.0f) {
                    yield return DefaultCanvas.GetDialogLock();
                    GameController.Pause();

                    GameController.StopAction(false);

                    yield return StartCoroutine(DialogBox.Show("All done with my lovely meal!"));
                    int energy = UnityEngine.Random.Range(1, 4);
                    int motivation = UnityEngine.Random.Range(3, 6);
                    int food = UnityEngine.Random.Range(7, 10);
                    GameController.Instance.Stats.CharacterEnergyInternal += energy;
                    GameController.Instance.Stats.CharacterFoodInternal += food;
                    GameController.Instance.Stats.CharacterMotivationInternal += motivation;
                    yield return StartCoroutine(ResultBox.Show("Wow, I feel so much better now!",
                        new[] {
                            string.Format("+{0} Food", food),
                            string.Format("+{0} Energy", energy),
                            string.Format("+{0} Motivation", motivation)
                        },
                        new[] { "icon_food", "icon_energy", "icon_motivation" }
                    ));

                    DefaultCanvas.ReleaseDialogLock();
                    GameController.Unpause();
                    yield break;
                }
            }
        }
    }
}
