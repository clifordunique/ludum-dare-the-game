﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HarmonicUnity;

public partial class GameController : MonoBehaviour
{
    IEnumerator GameActionCode() {
        AudioSource audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.clip = Resources.Load<AudioClip>("typing_normal");
        audioSource.loop = true;
        audioSource.Play();
        CharacterSprites.Switch("computer");

        Cleanup = () => {
            GameController.Instance.Stats.ProgressCode /= 2;
            audioSource.Stop();
            Destroy(audioSource);
        };
        ActionName = "coding";

        float rand = UnityEngine.Random.value;

        // Always takes some time to start working.
        yield return new WaitForSeconds(kWorkStartDelaySeconds);

        int helpChoice = -1;

        while (true) {
            yield return null;
            GameController.Instance.Stats.ProgressCode += Time.deltaTime;

            if (rand <= 0.5f) {
                // Normal feature.
                if (GameController.Instance.Stats.ProgressCode >= 4.0f) {
                    yield return DefaultCanvas.GetDialogLock();
                    GameController.Pause();

                    Stats.ProgressCode = 0.0f;
                    GameController.StopAction(false);

                    int programming = Mathf.RoundToInt(20 * Stats.CharacterProductivity());
                    int bugs = UnityEngine.Random.Range(0, 2);
                    Stats.GameProgramming += programming;
                    Stats.GameBugs += bugs;
                    if (bugs > 0) {
                        yield return ResultBox.Show(
                            "Success!  I finished coding a new feature for my game!",
                            new[] {
                                string.Format("+{0} Programming", programming),
                                string.Format("<color=#ff0000>+{0} Bugs</color>", bugs)
                            },
                            new[] { "icon_programming", "icon_bugs" }
                        );
                    } else {
                        yield return ResultBox.Show(
                            "Success!  I finished coding a new feature for my game!",
                            new[] { string.Format("+{0} Programming", programming) },
                            new[] { "icon_programming" }
                        );
                    }

                    DefaultCanvas.ReleaseDialogLock();
                    GameController.Unpause();
                    yield break;
                }
            } else if (rand < 0.75) {
                // Help feature.
                if (GameController.Instance.Stats.ProgressCode >= 3.0f && helpChoice == -1) {
                    yield return DefaultCanvas.GetDialogLock();
                    GameController.Pause();

                    yield return StartCoroutine(ChoiceBox.Show("I'm stuck trying to work on this feature...what should I do?", val => {
                        helpChoice = val;
                    }, "Read the Manual", "Search StackOverflow"));
                    if (helpChoice == 0) {
                        yield return StartCoroutine(DialogBox.Show("Let's see if the answer is in the help docs."));
                    } else {
                        yield return StartCoroutine(DialogBox.Show("I'll search online to see if someone else has run into this before."));
                    }

                    DefaultCanvas.ReleaseDialogLock();
                    GameController.Unpause();
                }

                if (GameController.Instance.Stats.ProgressCode >= 6.0f) {


                    if (helpChoice == 0) {

                        yield return DefaultCanvas.GetDialogLock();
                        GameController.Pause();

                        Stats.ProgressCode = 0.0f;
                        GameController.StopAction(false);

                        int programming = Mathf.RoundToInt(20 * Stats.CharacterProductivity());
                        Stats.GameProgramming += programming;
                        yield return ResultBox.Show(
                            "It took a while, but I worked through the issue!",
                            new[] {
                                string.Format("+{0} Programming", programming),
                            },
                            new[] { "icon_programming" }
                        );

                        DefaultCanvas.ReleaseDialogLock();
                        GameController.Unpause();
                        yield break;
                    } else {
                        yield return DefaultCanvas.GetDialogLock();
                        GameController.Pause();

                        Stats.ProgressCode = 0.0f;
                        GameController.StopAction(false);

                        int programming = Mathf.RoundToInt(30 * Stats.CharacterProductivity());
                        Stats.GameProgramming += programming;
                        yield return ResultBox.Show(
                            "I found the solution online!  And learned a bit more about coding along the way!",
                            new[] {
                                string.Format("+{0} Programming", programming),
                            },
                            new[] { "icon_programming" }
                        );

                        DefaultCanvas.ReleaseDialogLock();
                        GameController.Unpause();
                        yield break;

                    }
                }                
            } else {
                // Help feature.
                if (GameController.Instance.Stats.ProgressCode >= 4.0f && helpChoice == -1) {
                    yield return DefaultCanvas.GetDialogLock();
                    GameController.Pause();

                    yield return StartCoroutine(ChoiceBox.Show("This feature is probably going to take a bit longer...", val => {
                        helpChoice = val;
                    }, "Finish it ASAP", "Take your time"));
                    if (helpChoice == 0) {
                        yield return StartCoroutine(DialogBox.Show("I'll try and hack something together really quickly!"));
                    } else {
                        yield return StartCoroutine(DialogBox.Show("It'll take more work, but probably be worth it in the end."));
                    }

                    DefaultCanvas.ReleaseDialogLock();
                    GameController.Unpause();
                }

                if (helpChoice == 0 && GameController.Instance.Stats.ProgressCode >= 5.0f) {
                    yield return DefaultCanvas.GetDialogLock();
                    GameController.Pause();

                    Stats.ProgressCode = 0.0f;
                    GameController.StopAction(false);

                    int programming = Mathf.RoundToInt(40 * Stats.CharacterProductivity());
                    int bugs = UnityEngine.Random.Range(1, 6);
                    Stats.GameProgramming += programming;
                    Stats.GameBugs += bugs;
                    yield return ResultBox.Show(
                        "I was able to throw something together in no time!",
                        new[] {
                            string.Format("+{0} Programming", programming),
                            string.Format("<color=#ff0000>+{0} Bugs</color>", bugs)
                        },
                        new[] { "icon_programming", "icon_bugs" }
                    );

                    DefaultCanvas.ReleaseDialogLock();
                    GameController.Unpause();
                    yield break;
                } else if (helpChoice == 1 && GameController.Instance.Stats.ProgressCode >= 8.0f) {
                    yield return DefaultCanvas.GetDialogLock();
                    GameController.Pause();

                    Stats.ProgressCode = 0.0f;
                    GameController.StopAction(false);

                    int programming = Mathf.RoundToInt(40 * Stats.CharacterProductivity());
                    Stats.GameProgramming += programming;
                    yield return ResultBox.Show(
                        "Phew!  That took a while...",
                        new[] {
                            string.Format("+{0} Programming", programming),
                        },
                        new[] { "icon_programming" }
                    );

                    DefaultCanvas.ReleaseDialogLock();
                    GameController.Unpause();
                    yield break;

                }
                          
            }
        }
    }
}
