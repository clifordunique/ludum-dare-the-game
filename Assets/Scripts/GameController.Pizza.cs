﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HarmonicUnity;

public partial class GameController : MonoBehaviour
{
    IEnumerator GameActionPizza() {
        AudioSource audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.clip = Resources.Load<AudioClip>("microwave");
        audioSource.Play();
        CharacterSprites.Switch("microwave");

        Cleanup = () => {
            audioSource.Stop();
            Destroy(audioSource);
        };
        ActionName = "eating pizza";

        float rand = UnityEngine.Random.value;

        float progress = 0.0f;
        while (true) {
            yield return null;
            progress += Time.deltaTime;

            if (rand <= 1.0f) {
                // Normal feature.
                if (progress >= 3.0f) {
                    yield return DefaultCanvas.GetDialogLock();
                    GameController.Pause();

                    GameController.StopAction(false);

                    AudioManager.PlaySound("microwave_end");
                    yield return StartCoroutine(DialogBox.Show("Pizza's done!"));
                    if (UnityEngine.Random.value < 0.5f) {
                        int food = UnityEngine.Random.Range(3, 5);
                        GameController.Instance.Stats.CharacterFoodInternal += food;
                        yield return StartCoroutine(ResultBox.Show("Mmm...pizza...",
                            new[] { string.Format("+{0} Food", food) },
                            new[] { "icon_food" }
                        ));
                    } else {
                        int food = UnityEngine.Random.Range(2, 4);
                        GameController.Instance.Stats.CharacterFoodInternal += food;
                        yield return StartCoroutine(ResultBox.Show("It didn't really taste that good...",
                            new[] { string.Format("+{0} Food", food) },
                            new[] { "icon_food" }
                        ));
                    }

                    DefaultCanvas.ReleaseDialogLock();
                    GameController.Unpause();
                    yield break;
                }
            }
        }
    }
}
