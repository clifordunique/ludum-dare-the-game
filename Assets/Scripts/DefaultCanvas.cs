﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefaultCanvas : MonoBehaviour
{
    public static DefaultCanvas Instance;

    public bool DialogShowing = false;

    void Awake() {
        Instance = this;
    }

    public static IEnumerator GetDialogLock() {
        while (Instance.DialogShowing) {
            yield return null;
        }
        Instance.DialogShowing = true;
        yield break;
    }

    public static void ReleaseDialogLock() {
        Instance.DialogShowing = false;
    }
}
