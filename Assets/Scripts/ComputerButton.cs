﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using HarmonicUnity;

public class ComputerButton : MyButton, IPointerClickHandler
{
    public void OnPointerClick(PointerEventData eventData) {
        StartCoroutine(DoOnPointerClick());
    }

    IEnumerator DoOnPointerClick() {
        yield return DefaultCanvas.GetDialogLock();
        GameController.Pause();

        int result = -1;
        yield return StartCoroutine(ComputerBox.Show("What should I do next...?", val => {
            result = val;
        }));

        if (result == 6) {
            GameController.Unpause();
            DefaultCanvas.ReleaseDialogLock();

            yield break;
        }

        int stop = -1;
        yield return StartCoroutine(ChoiceBox.AskStopAction(val => {
            stop = val;
        }));

        if (stop == 1) {
            switch (result) {
            case 0:
                yield return StartCoroutine(DialogBox.Show("I'll try to code up another feature."));
                GameController.ChangeAction(GameController.GameAction.Code);
                break;
            case 1:
                yield return StartCoroutine(DialogBox.Show("I'll work on drawing some artwork."));
                GameController.ChangeAction(GameController.GameAction.Art);
                break;
            case 2:
                yield return StartCoroutine(DialogBox.Show("I'll work on some audio."));
                GameController.ChangeAction(GameController.GameAction.Audio);
                break;
            case 3:
                if (GameController.Instance.Stats.GameBugs <= 0) {
                    yield return StartCoroutine(DialogBox.Show("There are no bugs to fix!"));
                    break;
                }

                yield return StartCoroutine(DialogBox.Show("I'll try to fix some of the bugs in my code."));
                GameController.ChangeAction(GameController.GameAction.Bugs);
                break;
            case 4:
                yield return StartCoroutine(DialogBox.Show("I'll post a progress update to the LD blog."));
                GameController.ChangeAction(GameController.GameAction.Post);
                break;
            case 5:
                yield return StartCoroutine(DialogBox.Show("I'll read up what other people are up to so far."));
                GameController.ChangeAction(GameController.GameAction.Browse);
                break;
            default:
                Utils.Assert(false);
                break;
            }
        }

        GameController.Unpause();
        DefaultCanvas.ReleaseDialogLock();

        yield break;
    }
}
