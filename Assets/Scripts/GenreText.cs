﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GenreText : MonoBehaviour
{
    Text _text;

    void Awake() {
        _text = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update() {
        if (GameController.Instance.Stats.Genre == null) {
            _text.text = "[No Genre]";
        } else {
            _text.text = GameController.Instance.Stats.Genre.Name;
        }
    }
}
