﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HarmonicUnity;

public partial class GameController : MonoBehaviour
{
    IEnumerator GameActionPost() {
        AudioSource audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.clip = Resources.Load<AudioClip>("typing_normal");
        audioSource.loop = true;
        audioSource.Play();
        CharacterSprites.Switch("computer");

        Cleanup = () => {
            audioSource.Stop();
            Destroy(audioSource);
        };
        ActionName = "blogging";

        float rand = UnityEngine.Random.value;

        float progress = 0.0f;
        while (true) {
            yield return null;
            progress += Time.deltaTime;

            if (rand <= 1.0f) {
                // Normal feature.
                if (progress >= 5.0f) {
                    yield return DefaultCanvas.GetDialogLock();
                    GameController.Pause();

                    GameController.StopAction(false);

                    int motivation = UnityEngine.Random.Range(1, 4);
                    int community = Mathf.RoundToInt(20 * Stats.CharacterProductivity());
                    Stats.GameCommunity += community;
                    Stats.CharacterMotivationInternal += motivation;
                    yield return ResultBox.Show(
                        "I've finished my blog post!",
                        new[] {
                            string.Format("+{0} Community", community),
                            string.Format("+{0} Motivation", motivation)
                        },
                        new[] { "icon_community", "icon_motivation" }
                    );

                    DefaultCanvas.ReleaseDialogLock();
                    GameController.Unpause();
                    yield break;
                }
            }
        }
    }
}
