﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using HarmonicUnity;

public class BedButton : MyButton, IPointerClickHandler
{
    [SerializeField]
    AudioClip _sleepSound;

    public void OnPointerClick(PointerEventData eventData) {
        StartCoroutine(DoOnPointerClick());
    }

    IEnumerator DoOnPointerClick() {
        yield return DefaultCanvas.GetDialogLock();
        GameController.Pause();

        int result = -1;
        yield return StartCoroutine(BedBox.Show(val => {
            result = val;
        }));

        if (result == 4) {
            GameController.Unpause();
            DefaultCanvas.ReleaseDialogLock();
            yield break;
        }

        int stop = -1;
        yield return StartCoroutine(ChoiceBox.AskStopAction(val => {
            stop = val;
        }));

        if (stop == 1) {
            switch (result) {
            case 0:
                yield return StartCoroutine(DialogBox.Show("I've heard that meditation is a good way to stay focused!"));
                GameController.ChangeAction(GameController.GameAction.Meditate);
                break;
            case 1:
                GameController.StopAction(false);
                yield return StartCoroutine(DialogBox.Show("I'll just take a short nap..."));
                CharacterSprites.Switch("bed");
                AudioManager.FadeMusic(1.0f);
                ScreenFader.FadeOut(3.0f);
                AudioManager.PlaySound(_sleepSound);
                yield return new WaitForSecondsRealtime(4.0f);
                GameController.Instance.TimeLeft -= 3.0f;
                if (GameController.Instance.TimeLeft < 0.0f) {
                    GameController.Instance.TimeLeft = 0.0f;
                }
                ScreenFader.FadeIn(3.0f);
                yield return new WaitForSecondsRealtime(2.0f);
                AudioManager.PlayMusic("song1");
                yield return new WaitForSecondsRealtime(1.0f);

                if (Random.value < 0.5f) {
                    int energy = UnityEngine.Random.Range(4, 7);
                    int food = UnityEngine.Random.Range(1, 2);
                    GameController.Instance.Stats.CharacterEnergyInternal += energy;
                    GameController.Instance.Stats.CharacterFoodInternal -= food;
                    yield return StartCoroutine(ResultBox.Show("That was pretty effective!  But now I'm hungry...",
                        new[] {
                            string.Format("+{0} Energy", energy),
                            string.Format("<color=#ff0000>-{0} Food</color>", food)
                        },
                        new[] { "icon_energy", "icon_food" }
                    ));
                } else {
                    int energy = UnityEngine.Random.Range(3, 6);
                    GameController.Instance.Stats.CharacterEnergyInternal += energy;
                    yield return StartCoroutine(ResultBox.Show("I had this weird dream about a flying mushroom...",
                        new[] { string.Format("+{0} Energy", energy) },
                        new[] { "icon_energy" }
                    ));
                }
                CharacterSprites.Switch("computer");
                break;
            case 2:
                GameController.StopAction(false);
                yield return StartCoroutine(DialogBox.Show("I'm calling it a night."));
                CharacterSprites.Switch("bed");
                AudioManager.FadeMusic(1.0f);
                ScreenFader.FadeOut(3.0f);
                AudioManager.PlaySound(_sleepSound);
                yield return new WaitForSecondsRealtime(4.0f);
                GameController.Instance.TimeLeft -= 6.0f;
                if (GameController.Instance.TimeLeft < 0.0f) {
                    GameController.Instance.TimeLeft = 0.0f;
                }
                ScreenFader.FadeIn(3.0f);
                yield return new WaitForSecondsRealtime(2.0f);
                AudioManager.PlayMusic("song1");
                yield return new WaitForSecondsRealtime(1.0f);

                if (Random.value < 0.5f) {
                    yield return StartCoroutine(DialogBox.Show("Oh my gosh!  I just dreamed about the most amazing game design idea!"));
                    yield return StartCoroutine(DialogBox.Show("This is going to make my game perfect!  I just have to take the AI-controlled behaviors and..."));
                    yield return StartCoroutine(DialogBox.Show("...and...um...it was something to do with dynamic player input response...or...wait..."));
                    yield return StartCoroutine(DialogBox.Show("...nevermind, I completely forgot."));
                    int energy = UnityEngine.Random.Range(7, 10);
                    int motivation = UnityEngine.Random.Range(5, 8);
                    GameController.Instance.Stats.CharacterEnergyInternal += energy;
                    GameController.Instance.Stats.CharacterMotivationInternal += motivation;
                    yield return StartCoroutine(ResultBox.Show("At least I got a good night's rest...",
                        new[] { string.Format("+{0} Energy", energy), string.Format("+{0} Motivation", motivation) },
                        new[] { "icon_energy", "icon_motivation" }
                    ));
                } else {
                    int energy = UnityEngine.Random.Range(7, 10);
                    int motivation = UnityEngine.Random.Range(5, 8);
                    GameController.Instance.Stats.CharacterEnergyInternal += energy;
                    GameController.Instance.Stats.CharacterMotivationInternal += motivation;
                    yield return StartCoroutine(ResultBox.Show("Still...want to keep sleeping...",
                        new[] { string.Format("+{0} Energy", energy), string.Format("+{0} Motivation", motivation) },
                        new[] { "icon_energy", "icon_motivation" }
                    ));
                }
                CharacterSprites.Switch("computer");
                break;
            case 3:
                yield return StartCoroutine(DialogBox.Show("SLEEP IS FOR THE WEAK!"));
                GameController.ChangeAction(GameController.GameAction.Bounce);
                break;
            default:
                Utils.Assert(false);
                break;
            }
        }

        GameController.Unpause();
        DefaultCanvas.ReleaseDialogLock();

        yield break;
    }
}
