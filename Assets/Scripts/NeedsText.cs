﻿using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NeedsText : MonoBehaviour
{
    [SerializeField]
    Transform _itemParent;

    List<ResultItem> _items = new List<ResultItem>();

    [SerializeField]
    Text _prodText;

    void Awake() {
        for (int i = 0; i < _itemParent.childCount; ++i) {
            _items.Add(_itemParent.GetChild(i).GetComponent<ResultItem>());
        }
    }

    string GetColor(int need) {
        switch (need) {
        case 0:
            return "#800000";
        case 1:
            return "#ff0000";
        case 2:
            return "#ff4000";
        case 3:
            return "#ff8000";
        case 4:
            return "#ffc000";
        case 5:
            return "#ffff00";
        case 6:
            return "#c0ff00";
        case 7:
            return "#a0ff00";
        case 8:
            return "#80ff00";
        default:
            return "#00ff00";
        }
    }

    void Update() {
        Stats stats = GameController.Instance.Stats;
        string color;

        color = GetColor(stats.CharacterFood);
        _items[0].Set(string.Format("<color={0}>Food: {1}</color>", color, stats.CharacterFood), "icon_food");
        color = GetColor(stats.CharacterEnergy);
        _items[1].Set(string.Format("<color={0}>Energy: {1}</color>", color, stats.CharacterEnergy), "icon_energy");
        color = GetColor(stats.CharacterMotivation);
        _items[2].Set(string.Format("<color={0}>Motivation: {1}</color>", color, stats.CharacterMotivation), "icon_motivation");



        int productivity = Mathf.RoundToInt(GameController.Instance.Stats.CharacterProductivity() * 100);
        _prodText.text = string.Format("Productivity: {0}%", productivity);
    }
}
