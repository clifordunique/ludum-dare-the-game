﻿using UnityEngine;

public static class ThemeGenerator
{
    static readonly string[] kThemes = {
        "Into the unknown",
        "Unconventional physics",
        "Nothing is permanent",
        "Time manipulation",
        "Apocalypse",
        "Alchemy",
        "Teleportation",
        "Upgrade yourself",
        "Salvage and repair",
        "Running out of space",
        "Weaponless",
        "Only 5 minutes",
        "Advancing technology",
        "Small world",
        "You are the projectile",
        "Afterlife",
        "Underground",
        "The rules change as you play",
        "One resource",
        "Save yourself, not the world",
        "Leave something behind",
        "Assemble",
        "Get me out of here!",
        "Adaptation",
        "Two colors",
        "Artificial life",
        "Guardian",
        "Infinite loop",
        "One room",
        "Stronger together",
        "Surveillance",
        "Playing both sides",
        "Isolation",
        "Chain reaction",
        "Time kills you",
        "Simplicity",
        "Escape the game",
        "Natural selection",
        "Your enemy is your weapon",
        "Control the environment not the characters",
        "Parallel worlds",
        "Indirect control",
        "Fix it",
        "Wait, are we the bad guys?",
        "Choose one",
        "Inconvenient superpowers",
        "Light and dark",
        "Fusion",
    };

    public static string GetRandom() {
        int index = Random.Range(0, kThemes.Length);
        return kThemes[index];
    }
}
