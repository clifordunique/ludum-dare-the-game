﻿using System.Text;
using System;
using System.Collections;
using System.Collections.Generic;
using HarmonicUnity;
using UnityEngine;
using UnityEngine.UI;

public class RefreshBox : GenericBox
{
    public static RefreshBox Instance;

    int _refreshCount;

    [SerializeField]
    Text _loadText;

    [SerializeField]
    GameObject _button;

    public static IEnumerator Show() {
        Instance.gameObject.SetActive(true);
        Instance._button.SetActive(false);
        yield return Instance.StartCoroutine(Instance.ShowText("Click the Refresh button until the theme comes up!"));

        Instance._button.SetActive(true);

        while (Instance._refreshCount < 12) {
            yield return null;
        }

        Instance.gameObject.SetActive(false);
        yield break;
    }

    public void Refresh() {
        _refreshCount++;

        StartCoroutine(Flash());
    }

    IEnumerator Flash() {
        Instance._loadText.text = "";
        yield return new WaitForSecondsRealtime(UnityEngine.Random.value / 2);
        Instance._loadText.text = "Loading ";
        yield return null;
        Instance._loadText.text = "Loading http://ludumdare.com";
        yield return null;
        Instance._loadText.text = "Loading http://ludumdare.com...";
        yield break;
    }

    protected override void Awake() {
        base.Awake();
        Instance = this;
    }
}
